from django.urls import path
from receipts.views import (
    receipts,
    create_receipt,
    list_category,
    list_account,
    create_account,
    create_category,

)


urlpatterns = [
    path("", receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", list_category, name="category_list"),
    path("accounts/", list_account, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
]
