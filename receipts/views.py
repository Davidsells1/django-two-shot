from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, AccountForm, ExpenseForm
# Create your views here.


@login_required
def receipts(request):
    items = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": items

    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )
        form.fields["account"].queryset = Account.objects.filter(
            owner=request.user
        )
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def list_category(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/list_category.html", context)


@login_required
def list_account(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/list_account.html", context)


@login_required
def create_account(request):
    if request.method == "GET":
        form = AccountForm()
    else:
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)


@login_required
def create_category(request):
    if request.method == "GET":
        form = ExpenseForm()
    else:
        form = ExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)
